------------------------------------------------------------------------------
--                                                                          --
--  Copyright (c) 2016, Jasmin St-Laurent                                   --
--  All rights reserved.                                                    --
--                                                                          --
--  Redistribution and use in source and binary forms, with or without      --
--  modification, are permitted provided that the following conditions are  --
--  met:                                                                    --
--     1. Redistributions of source code must retain the above copyright    --
--        notice, this list of conditions and the following disclaimer.     --
--     2. Redistributions in binary form must reproduce the above copyright --
--        notice, this list of conditions and the following disclaimer in   --
--        the documentation and/or other materials provided with the        --
--        distribution.                                                     --
--     3. Neither the name of the copyright holder nor the names of its     --
--        contributors may be used to endorse or promote products derived   --
--        from this software without specific prior written permission.     --
--                                                                          --
--   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    --
--   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      --
--   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  --
--   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   --
--   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, --
--   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       --
--   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  --
--   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  --
--   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    --
--   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  --
--   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   --
--                                                                          --
------------------------------------------------------------------------------

------------------------------------------------------------------------------
--
-- Specification of an FSM blinking an LED
--
------------------------------------------------------------------------------


package FSM.Blink is

   type Blink_FSM is new FSM_Type with private;

   type State_Handler is access procedure (FSM : in out Blink_FSM; Event : in Event_Type);

   overriding procedure Init (FSM : in out Blink_FSM);
   overriding procedure Handle_Event(FSM : in out Blink_FSM; Event : in Event_Type);

private
   type Blink_FSM is new FSM_Type with
     record
         State : State_Handler;
         -- Ajouter un time event
      end record;

   procedure LED_On_SH (FSM : in out Blink_FSM; Event : in Event_Type);
   procedure LED_Off_SH (FSM : in out Blink_FSM; Event : in Event_Type);
   procedure Transition(FSM : in out Blink_FSM; New_State : in State_Handler);

end FSM.Blink;
